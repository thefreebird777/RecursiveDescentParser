import java.util.Stack;

/**
 * Created by Ian on 9/18/2016.
 */
public class LL1 {

    StringBuilder sb;
    Stack<String> stack = new Stack<String>();
    Stack<String> ckStack = new Stack<String>();
    String expression = " ";
    String token = "";
    int length = 0;
    int open = 0;
    int close = 0;

    /**
     * Constructor
     */
    public LL1(String input) {
        this.expression = input;
        length = expression.length();
        sb = new StringBuilder(expression);
        char q;
        q = sb.charAt(length - 1);
        if (q != '$') {
            expression += "$";
            sb = new StringBuilder(expression);
        }
    }

    /**
     * gets a token from the input string
     *
     * @return token
     */
    public String getToken() {
        token = "";
        for (int i = 0; i < length; i++) {
            if (sb.charAt(0) != '+' && sb.charAt(0) != '-' &&
                    sb.charAt(0) != '*' && sb.charAt(0) != '/'
                    && sb.charAt(0) != '(' && sb.charAt(0) != ')' && sb.charAt(0) != '$') {
                token += sb.charAt(0);
                sb.deleteCharAt(0);
            } else {
                if (token.isEmpty()) {
                    token += sb.charAt(0);
                    sb.deleteCharAt(0);
                }
                break;
            }
        }
        return token;
    }

    /**
     * compares parsed stack with original string
     *
     * @return boolean of success or failure
     */
    public boolean checkStack() {
        boolean ck = false;
        if (open == 0) {
            token = "";
            sb = new StringBuilder(expression);
            while (!sb.toString().isEmpty()) {
                ckStack.push(getToken());
            }
            String a;
            String b;

            while (!stack.isEmpty()) {
                a = stack.pop();
                b = ckStack.pop();
                if (a.equals(b)) {
                    ck = true;
                } else {
                    ck = false;
                }
            }
        } else {
            ck = false;
        }
        return ck;
    }

//start recursive descent algorithm:

    public boolean E() {
        if (T() == false) {
            error();
        }
        if (Eprime() == false) {
            error();
        }
        return true;
    }

    public boolean Eprime() {
        if (stack.peek().equals(token)) {
            getToken();
        }
        switch (token) {
            case "+":
                stack.push(token);
                if (T() == false) {
                    error();
                }
                if (Eprime() == false) {
                    error();
                }
                return true;
            case "-":
                stack.push(token);
                if (T() == false) {
                    error();
                }
                if (Eprime() == false) {
                    error();
                }
                return true;
            case "*":
                return true;
            case "/":
                return true;
            case "(":
                error();
                return false;
            case ")":
                close++;
                return closingPara();
            case "$":
                stack.push(token);
                if (checkStack() == true) {
                    success();
                } else {
                    error();
                }
                return true;
        }
        return false;
    }


    public boolean T() {
        if (F() == false) {
            error();
        }
        if (Tprime() == false) {
            error();
        }
        return true;
    }

    public boolean Tprime() {
        if (stack.peek().equals(token)) {
            getToken();
        }
        switch (token) {
            case "+":

                return true;
            case "-":

                return true;
            case "*":
                stack.push(token);
                if (F() == false) {
                    error();
                }
                if (Tprime() == false) {
                    error();
                }
                return true;
            case "/":
                stack.push(token);
                if (F() == false) {
                    error();
                }
                if (Tprime() == false) {
                    error();
                }
                return true;
            case "(":
                error();
                return false;
            case ")":
                close++;
                return closingPara();
            case "$":
                stack.push(token);
                if (checkStack() == true) {
                    success();
                } else {
                    error();
                }
                return true;
        }
        return false;
    }

    public boolean F() {
        getToken();
        switch (token) {
            case "+":
                error();
                return false;
            case "-":
                error();
                return false;
            case "*":
                error();
                return false;
            case "/":
                error();
                return false;
            case "(":
                open++;
                stack.push(token);
                if (E() == false) {
                    error();
                }
                return true;
            case ")":
                error();
                return false;
            case "$":
                error();
                return false;
        }
        if (isInt(token) == true) {
            stack.push(token);
            return true;
        }
        return false;
    }

//end recursive descent algorithm

    /**
     * checks to see if closing parenthesis has a matching opening parenthesis
     *
     * @return true for yes false for no
     */
    public boolean closingPara() {
        if (close <= open) {
            open--;
            close--;
            stack.push(token);
            return true;
        } else {
            error();
            return false;
        }
    }

    /**
     * checks if input is an integer
     *
     * @param str
     * @return true if integer false if not
     */
    public static boolean isInt(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    /**
     * stops program if the user inputs an invalid expression
     */
    public void error() {
        System.out.println("No");
        System.exit(0);
    }

    /**
     * stops program if it is a success
     */
    public void success() {
        System.out.println("Yes");
        System.exit(0);
    }

    /**
     * starts algorithm
     */
    public void run() {
        E();
    }

    /**
     * main method, accepts user argument
     *
     * @param args
     */
    public static void main(String[] args) {
        LL1 parser = new LL1(args[0]);
        parser.run();
    }
}
